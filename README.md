# README #

This repository contains a sample C# application written using Visual Studio Community Edition.

### What is this repository for? ###

This project (Visual Studio Solution) demonstrates (crudely) the use of a number of form controls, and reading and writing to an XML file.

There is some sanitisation of what is allowed for input, but it is not menat to show everything at this time.

Version 1.0

### How do I get set up? ###

Download the file. Uncompress it into a folder, and open in Visual Studio 2015 or later.

Compile and run (CTRL+F5).

### Contribution guidelines ###

* Writing tests - TBA
* Code review - TBA
* Other guidelines - TBA

### Who do I talk to? ###

* Adrian Gould (Lecturer in IT and Multimedia)
North Metropolitan TAFE, Perth, Western Australia
