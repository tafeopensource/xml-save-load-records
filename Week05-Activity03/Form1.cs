﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace TAFEos_XMLSaveLoad
{
    public partial class frmListADS : Form
    {
        private const string xmlFileName = "c:\\temp\\acmeEmployees.xml";

        private List<Employee> acmeEmployee = new List<Employee>();

        public frmListADS()
        {
            InitializeComponent();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            bool hasData = true;
            Employee newEmployee = new Employee();
            string errors = "";

            newEmployee.fullName = txtName.Text;
            newEmployee.position = txtPosition.Text;
            newEmployee.status = cboStatus.Text;

            var checkedButton = grpGender.Controls.OfType<RadioButton>()
                                      .FirstOrDefault(rb => rb.Checked);

            newEmployee.gender = checkedButton.Text;

            if (String.IsNullOrEmpty(txtName.Text))
            {
                errors = errors + "Please enter a full name" + (char)10;
                hasData = false;
            }

            if (String.IsNullOrEmpty(txtPosition.Text))
            {
                errors = errors + "Please enter a position" + (char)10;
                hasData = false;
            }

            if (String.IsNullOrEmpty(cboStatus.Text))
            {
                errors = errors + "Please select a status" + (char)10;
                hasData = false;
            }

            if (!hasData)
            {
                MessageBox.Show(errors);
                return;
            }

            bool duplicateFound = acmeEmployee.Exists(
                x => x.fullName == txtName.Text);

            if (hasData && !duplicateFound)
            {
                acmeEmployee.Add(newEmployee);
                ResetDetails();
                DisplayRecords();
            }
            else
            {
                MessageBox.Show("Sorry, that employee exists");
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (lstEmployees.SelectedIndex == -1)
            {
                MessageBox.Show("Select an employee to remove");
            }
            else
            {
                string curItem = lstEmployees.SelectedItem.ToString();
                int indx = lstEmployees.FindString(curItem);
                acmeEmployee.RemoveAt(indx);
                DisplayRecords();
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            Employee updatedEmployee = new Employee();
            updatedEmployee.fullName = txtName.Text;
            updatedEmployee.position = txtPosition.Text;
            updatedEmployee.status = cboStatus.Text;
            var checkedButton = grpGender.Controls.OfType<RadioButton>()
                                      .FirstOrDefault(rb => rb.Checked);
            updatedEmployee.gender = checkedButton.Text;

            if (lstEmployees.SelectedIndex == -1)
            {
                MessageBox.Show("Please select an employee to update");
            }
            else
            {
                string curItem = lstEmployees.SelectedItem.ToString();
                int indx = lstEmployees.FindString(curItem);
                acmeEmployee[indx].fullName = updatedEmployee.fullName;
                acmeEmployee[indx].position = updatedEmployee.position;
                acmeEmployee[indx].status = updatedEmployee.status;
                acmeEmployee[indx].gender = updatedEmployee.gender;
                ResetDetails();
                DisplayRecords();
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ResetDetails();
        }

        private void btnSort_Click(object sender, EventArgs e)
        {
            acmeEmployee.Sort();
            DisplayRecords();
        }

        private void lstEmployees_MouseClick(object sender, MouseEventArgs e)
        {
            if (lstEmployees.SelectedIndex == -1)
            {
                MessageBox.Show("Please select a record in the list");
            }
            else
            {
                string curItem = lstEmployees.SelectedItem.ToString();
                int indx = lstEmployees.FindString(curItem);
                lstEmployees.SetSelected(indx, true);
                txtName.Text = acmeEmployee[indx].fullName;
                txtPosition.Text = acmeEmployee[indx].position;
                cboStatus.Text = acmeEmployee[indx].status;
                setGenderRadio(acmeEmployee[indx].gender);
            }
        }

        public void setGenderRadio(string gender)
        {
            var genderControls = grpGender.Controls.OfType<RadioButton>();
            foreach (RadioButton radBtn in genderControls)
            {
                if (radBtn.Text == gender)
                {
                    radBtn.Checked = true;
                }
                else
                {
                    radBtn.Checked = false;
                }
            }
        }

        public void DisplayRecords()
        {
            lstEmployees.Items.Clear();
            foreach (var emp in acmeEmployee)
            {
                lstEmployees.Items.Add(emp.fullName + "\t: " + emp.position);
            }
        }

        private void ResetDetails()
        {
            txtName.Text = "";
            txtPosition.Text = "";
            cboStatus.Text = "Active";
        }

        private void btnSaveXML_Click(object sender, EventArgs e)
        {
            saveToXML();
        }

        private void btnLoadXML_Click(object sender, EventArgs e)
        {
            loadFromXML();
            DisplayRecords();
        }

        private void frmListADS_FormClosing(object sender, FormClosingEventArgs e)
        {
            saveToXML();
        }

        private void saveToXML()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<Employee>));
            FileStream stream = File.Create(xmlFileName);

            using (stream)
            {
                serializer.Serialize(stream, acmeEmployee);
            }

            stream.Close();
        }

        private void loadFromXML()
        {
            XmlSerializer serializer = new XmlSerializer(typeof(List<Employee>));
            FileStream stream = File.OpenRead(xmlFileName);
            using (stream)
            {
                acmeEmployee = (List<Employee>)serializer.Deserialize(stream);
            }
            stream.Close();
        }

        private void frmListADS_Load(object sender, EventArgs e)
        {
            loadFromXML();
            DisplayRecords();
        }
    } // end frmListADS class

    // define Empoloyee class
    public class Employee : IComparable<Employee>
    {
        [XmlElement("FullName")]
        public string fullName
        {
            get; // name = emp.fullname
            set; // emp.fullname = "fred"
        }

        [XmlElement("Position")]
        public string position
        {
            get;
            set;
        }

        [XmlElement("Status")]
        public string status
        {
            get;
            set;
        }

        [XmlElement("Gender")]
        public string gender
        {
            get;
            set;
        }

        // Simple compare method for sorting...
        public int CompareTo(Employee other)
        {
            // compare THIS fullname to the other's fullname
            return this.fullName.CompareTo(other.fullName);
        }
    }
}